ai3/go-common
===

Common code for ai3 services and tools.

A quick overview of the contents:

* [client](clientutil/) and [server](serverutil/) HTTP-based
  "RPC" implementation, just JSON POST requests but with retries,
  backoff, timeouts, tracing, etc.

* [server implementation of a line-based protocol over a UNIX socket](unix/)

* a [LDAP connection pool](ldap/)

* a [password hashing library](pwhash/) that uses fancy advanced
  crypto by default but is also backwards compatible with old
  libc crypto.

* utilities to [manage encryption keys](userenckey/), themselves
  encrypted with a password and a KDF.

