package client

import (
	"context"
	"net/textproto"

	"git.autistici.org/id/auth"
)

var DefaultSocketPath = "/run/auth/socket"

type Client interface {
	Authenticate(context.Context, *auth.Request) (*auth.Response, error)
}

type socketClient struct {
	socketPath string
	codec      auth.Codec
}

func New(socketPath string) Client {
	return &socketClient{
		socketPath: socketPath,
		codec:      auth.DefaultCodec,
	}
}

func (c *socketClient) Authenticate(ctx context.Context, req *auth.Request) (*auth.Response, error) {
	// Create the connection outside of the timed goroutine, so
	// that we can call Close() on exit regardless of the reason:
	// this way, when a timeout occurs or the context is canceled,
	// the pending request terminates immediately.
	conn, err := textproto.Dial("unix", c.socketPath)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	// Make space in the channel for at least one element, or we
	// will leak a goroutine whenever the authentication request
	// times out.
	done := make(chan error, 1)
	var resp auth.Response

	go func() {
		defer close(done)

		// Write the auth command to the connection.
		if err := conn.PrintfLine("auth %s", string(c.codec.Encode(req))); err != nil {
			done <- err
			return
		}

		// Read the response.
		line, err := conn.ReadLineBytes()
		if err != nil {
			done <- err
			return
		}
		if err := c.codec.Decode(line, &resp); err != nil {
			done <- err
			return
		}
		done <- nil
	}()

	// Wait for the call to terminate, or the context to time out,
	// whichever happens first.
	select {
	case err := <-done:
		return &resp, err
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}
