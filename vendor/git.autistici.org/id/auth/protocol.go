package auth

import (
	"fmt"

	"github.com/tstranex/u2f"
)

// DeviceInfo holds information about the client device. We use a
// simple persistent cookie to track the same client device across
// multiple session.
type DeviceInfo struct {
	ID         string `json:"id"`
	RemoteAddr string `json:"remote_addr"`
	RemoteZone string `json:"remote_zone"`
	UserAgent  string `json:"user_agent"`
	Browser    string `json:"browser"`
	OS         string `json:"os"`
	Mobile     bool   `json:"mobile"`
}

func (d *DeviceInfo) encodeToMap(m map[string]string, prefix string) {
	m[prefix+"id"] = d.ID
	m[prefix+"remote_addr"] = d.RemoteAddr
	m[prefix+"remote_zone"] = d.RemoteZone
	m[prefix+"browser"] = d.Browser
	m[prefix+"os"] = d.OS
	m[prefix+"user_agent"] = d.UserAgent
	if d.Mobile {
		m[prefix+"mobile"] = "true"
	} else {
		m[prefix+"mobile"] = "false"
	}
}

func decodeDeviceInfoFromMap(m map[string]string, prefix string) *DeviceInfo {
	if _, ok := m[prefix+"id"]; !ok {
		return nil
	}
	return &DeviceInfo{
		ID:         m[prefix+"id"],
		RemoteAddr: m[prefix+"remote_addr"],
		RemoteZone: m[prefix+"remote_zone"],
		Browser:    m[prefix+"browser"],
		OS:         m[prefix+"os"],
		UserAgent:  m[prefix+"user_agent"],
		Mobile:     m[prefix+"mobile"] == "true",
	}
}

// Request to authenticate a user. It supports multiple methods for
// authentication including challenge-response 2FA.
type Request struct {
	Service     string
	Username    string
	Password    []byte
	OTP         string
	U2FAppID    string
	U2FResponse *u2f.SignResponse
	DeviceInfo  *DeviceInfo
}

func (r *Request) EncodeToMap(m map[string]string, prefix string) {
	m[prefix+"service"] = r.Service
	m[prefix+"username"] = r.Username
	m[prefix+"password"] = string(r.Password)

	if r.OTP != "" {
		m[prefix+"otp"] = r.OTP
	}
	if r.U2FAppID != "" {
		m[prefix+"u2f_app_id"] = r.U2FAppID
	}
	if r.U2FResponse != nil {
		encodeU2FResponseToMap(r.U2FResponse, m, prefix+"u2f_response.")
	}
	if r.DeviceInfo != nil {
		r.DeviceInfo.encodeToMap(m, prefix+"device.")
	}
}

func (r *Request) DecodeFromMap(m map[string]string, prefix string) {
	r.Service = m[prefix+"service"]
	r.Username = m[prefix+"username"]
	r.Password = []byte(m[prefix+"password"])
	r.OTP = m[prefix+"otp"]
	r.U2FAppID = m[prefix+"u2f_app_id"]
	r.U2FResponse = decodeU2FResponseFromMap(m, prefix+"u2f_response.")
	r.DeviceInfo = decodeDeviceInfoFromMap(m, prefix+"device.")
}

// UserInfo contains optional user information that may be useful to
// authentication endpoints.
type UserInfo struct {
	Email  string
	Shard  string
	Groups []string
}

func encodeStringList(m map[string]string, prefix string, l []string) {
	for i, elem := range l {
		m[fmt.Sprintf("%s.%d.", prefix, i)] = elem
	}
}

func decodeStringList(m map[string]string, prefix string) (out []string) {
	i := 0
	for {
		s, ok := m[fmt.Sprintf("%s.%d.", prefix, i)]
		if !ok {
			break
		}
		out = append(out, s)
		i++
	}
	return
}

func (u *UserInfo) EncodeToMap(m map[string]string, prefix string) {
	if u.Email != "" {
		m[prefix+"email"] = u.Email
	}
	if u.Shard != "" {
		m[prefix+"shard"] = u.Shard
	}
	encodeStringList(m, prefix+"group", u.Groups)
}

func decodeUserInfoFromMap(m map[string]string, prefix string) *UserInfo {
	u := UserInfo{
		Email:  m[prefix+"email"],
		Shard:  m[prefix+"shard"],
		Groups: decodeStringList(m, prefix+"group"),
	}
	if u.Email == "" && u.Shard == "" && len(u.Groups) == 0 {
		return nil
	}
	return &u
}

// Response to an authentication request.
type Response struct {
	Status         Status
	TFAMethods     []TFAMethod
	U2FSignRequest *u2f.WebSignRequest
	UserInfo       *UserInfo
}

// Has2FAMethod checks for the presence of a two-factor authentication
// method in the Response.
func (r *Response) Has2FAMethod(needle TFAMethod) bool {
	for _, m := range r.TFAMethods {
		if m == needle {
			return true
		}
	}
	return false
}

func encodeTFAMethodList(m map[string]string, prefix string, l []TFAMethod) {
	if len(l) == 0 {
		return
	}
	tmp := make([]string, 0, len(l))
	for _, el := range l {
		tmp = append(tmp, string(el))
	}
	encodeStringList(m, prefix, tmp)
}

func decodeTFAMethodList(m map[string]string, prefix string) []TFAMethod {
	l := decodeStringList(m, prefix)
	if len(l) == 0 {
		return nil
	}
	out := make([]TFAMethod, 0, len(l))
	for _, el := range l {
		out = append(out, TFAMethod(el))
	}
	return out
}

func (r *Response) EncodeToMap(m map[string]string, prefix string) {
	m[prefix+"status"] = r.Status.String()
	encodeTFAMethodList(m, prefix+"2fa_methods", r.TFAMethods)
	if r.U2FSignRequest != nil {
		encodeU2FSignRequestToMap(r.U2FSignRequest, m, prefix+"u2f_req.")
	}
	if r.UserInfo != nil {
		r.UserInfo.EncodeToMap(m, prefix+"user.")
	}
}

func (r *Response) DecodeFromMap(m map[string]string, prefix string) {
	r.Status = parseAuthStatus(m[prefix+"status"])
	r.TFAMethods = decodeTFAMethodList(m, prefix+"2fa_methods")
	r.U2FSignRequest = decodeU2FSignRequestFromMap(m, prefix+"u2f_req.")
	r.UserInfo = decodeUserInfoFromMap(m, prefix+"user.")
}

// TFAMethod is a hint provided to the caller with the type of 2FA
// method that is available for authentication.
type TFAMethod string

const (
	TFAMethodNone = ""
	TFAMethodOTP  = "otp"
	TFAMethodU2F  = "u2f"
)

// Status of an authentication request.
type Status int

const (
	StatusOK = iota
	StatusInsufficientCredentials
	StatusError
)

func (s Status) String() string {
	switch s {
	case StatusOK:
		return "ok"
	case StatusInsufficientCredentials:
		return "insufficient_credentials"
	case StatusError:
		return "error"
	default:
		return fmt.Sprintf("unknown(%d)", int(s))
	}
}

func parseAuthStatus(s string) Status {
	switch s {
	case "ok":
		return StatusOK
	case "insufficient_credentials":
		return StatusInsufficientCredentials
	default:
		return StatusError
	}
}

func (s Status) Error() string {
	return s.String()
}

// Miscellaneous serializers for external objects.

func encodeU2FResponseToMap(resp *u2f.SignResponse, m map[string]string, prefix string) {
	m[prefix+"key_handle"] = resp.KeyHandle
	m[prefix+"signature_data"] = resp.SignatureData
	m[prefix+"client_data"] = resp.ClientData
}

func decodeU2FResponseFromMap(m map[string]string, prefix string) *u2f.SignResponse {
	if _, ok := m[prefix+"key_handle"]; !ok {
		return nil
	}
	return &u2f.SignResponse{
		KeyHandle:     m[prefix+"key_handle"],
		SignatureData: m[prefix+"signature_data"],
		ClientData:    m[prefix+"client_data"],
	}
}

func encodeU2FSignRequestToMap(req *u2f.WebSignRequest, m map[string]string, prefix string) {
	m[prefix+"app_id"] = req.AppID
	m[prefix+"challenge"] = req.Challenge
	for i, key := range req.RegisteredKeys {
		encodeU2FRegisteredKeyToMap(key, m, fmt.Sprintf("%sregistered_keys.%d.", prefix, i))
	}
}

func encodeU2FRegisteredKeyToMap(key u2f.RegisteredKey, m map[string]string, prefix string) {
	m[prefix+"version"] = key.Version
	m[prefix+"key_handle"] = key.KeyHandle
	m[prefix+"app_id"] = key.AppID
}

func decodeU2FSignRequestFromMap(m map[string]string, prefix string) *u2f.WebSignRequest {
	if _, ok := m[prefix+"app_id"]; !ok {
		return nil
	}
	req := u2f.WebSignRequest{
		AppID:     m[prefix+"app_id"],
		Challenge: m[prefix+"challenge"],
	}
	i := 0
	for {
		key := decodeU2FRegisteredKeyFromMap(m, fmt.Sprintf("%sregistered_keys.%d.", prefix, i))
		if key == nil {
			break
		}
		req.RegisteredKeys = append(req.RegisteredKeys, *key)
		i++
	}
	return &req
}

func decodeU2FRegisteredKeyFromMap(m map[string]string, prefix string) *u2f.RegisteredKey {
	if _, ok := m[prefix+"version"]; !ok {
		return nil
	}
	return &u2f.RegisteredKey{
		Version:   m[prefix+"version"],
		KeyHandle: m[prefix+"key_handle"],
		AppID:     m[prefix+"app_id"],
	}
}
