package keystore

type OpenRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	TTL      int    `json:"ttl"`
}

type OpenResponse struct{}

type GetRequest struct {
	Username  string `json:"username"`
	SSOTicket string `json:"sso_ticket"`
}

type GetResponse struct {
	Key []byte `json:"key"`
}

type CloseRequest struct {
	Username string `json:"username"`
}

type CloseResponse struct{}
