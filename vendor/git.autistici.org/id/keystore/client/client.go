package client

import (
	"context"

	"git.autistici.org/ai3/go-common/clientutil"

	"git.autistici.org/id/keystore"
)

// Client for the keystore API.
type Client interface {
	Open(context.Context, string, string, string, int) error
	Get(context.Context, string, string, string) ([]byte, error)
	Close(context.Context, string, string) error
}

type ksClient struct {
	be clientutil.Backend
}

// New returns a new Client with the given Config. Use this when the
// keystore service runs on a single global instance.
func New(config *clientutil.BackendConfig) (*ksClient, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &ksClient{be}, nil
}

func (c *ksClient) Open(ctx context.Context, shard, username, password string, ttl int) error {
	req := keystore.OpenRequest{
		Username: username,
		Password: password,
		TTL:      ttl,
	}
	var resp keystore.OpenResponse
	return c.be.Call(ctx, shard, "/api/open", &req, &resp)
}

func (c *ksClient) Get(ctx context.Context, shard, username, ssoTicket string) ([]byte, error) {
	req := keystore.GetRequest{
		Username:  username,
		SSOTicket: ssoTicket,
	}
	var resp keystore.GetResponse
	err := c.be.Call(ctx, shard, "/api/get_key", &req, &resp)
	return resp.Key, err
}

func (c *ksClient) Close(ctx context.Context, shard, username string) error {
	req := keystore.CloseRequest{
		Username: username,
	}
	var resp keystore.CloseResponse
	return c.be.Call(ctx, shard, "/api/close", &req, &resp)
}
