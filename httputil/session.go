package httputil

import (
	"encoding/gob"
	"time"
)

// ExpiringSession is a session with server-side expiration check.
// Session data is saved in signed, encrypted cookies in the
// browser. We'd like these cookies to expire when a certain amount of
// time passes, or when the user closes the browser. We trust the
// browser for the latter, but we enforce time-based expiration on the
// server.
type ExpiringSession struct {
	Expiry time.Time
}

// NewExpiringSession returns a session that is valid for the given
// duration.
func NewExpiringSession(ttl time.Duration) *ExpiringSession {
	return &ExpiringSession{
		Expiry: time.Now().Add(ttl),
	}
}

// Valid returns true if the session has not expired yet.
// It can be called with a nil receiver.
func (e *ExpiringSession) Valid() bool {
	return e != nil && time.Now().Before(e.Expiry)
}

func init() {
	gob.Register(&ExpiringSession{})
}

