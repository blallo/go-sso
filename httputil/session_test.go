package httputil

import (
	"bytes"
	"encoding/gob"
	"reflect"
	"testing"
	"time"
)

func TestExpiringSession(t *testing.T) {
	type mySession struct {
		*ExpiringSession
		Data string
	}
	s := &mySession{
		ExpiringSession: NewExpiringSession(60 * time.Second),
		Data:            "data",
	}

	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(s); err != nil {
		t.Fatal("encode:", err)
	}
	var s2 mySession
	if err := gob.NewDecoder(&buf).Decode(&s2); err != nil {
		t.Fatal("decode:", err)
	}
	if !reflect.DeepEqual(s.Data, s2.Data) {
		t.Fatalf("sessions differ: %+v vs %+v", s, &s2)
	}
}
