package device

import "github.com/gorilla/sessions"

const aVeryLongTimeInSeconds = 10 * 365 * 86400

func newStore(authKey []byte) sessions.Store {
	// No encryption, long-term lifetime cookie.
	store := sessions.NewCookieStore(authKey, nil)
	store.Options = &sessions.Options{
		Path:     "/",
		HttpOnly: true,
		Secure:   true,
		MaxAge:   aVeryLongTimeInSeconds,
	}
	return store
}
