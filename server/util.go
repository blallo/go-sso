package server

// Returns the intersection of two string lists (in O(N^2) time).
func intersectGroups(a, b []string) []string {
	var out []string
	for _, aa := range a {
		for _, bb := range b {
			if aa == bb {
				out = append(out, aa)
				break
			}
		}
	}
	return out
}
