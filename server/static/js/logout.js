var idlogout = {};

idlogout.get_services = function() {
    return JSON.parse($('#services').attr('data-services'));
};

idlogout.logout_service = function(idx, service) {
    console.log('logging out of ' + service.name);
    $.ajax({
        type: 'GET',
        url: service.url,
        contentType: 'text/plain',
        xhrFields: {
            withCredentials: true
        },
        success: function() {
            $('#status_'+idx).addClass('logout-status-ok').text('OK');
            console.log('successful logout for ' + service.name);
        },
        error: function() {
            $('#status_'+idx).addClass('logout-status-error').text('ERROR');
            console.log('error logging out of ' + service.name);
        }
    });
};

idlogout.logout = function() {
    var services = idlogout.get_services();
    $.each(services, function(index, arg) {
        idlogout.logout_service(index, arg);
    });
};

$(function() {
    $('.logout-status').show();
    idlogout.logout();
});
