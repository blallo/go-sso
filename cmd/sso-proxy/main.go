package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gopkg.in/yaml.v2"

	"git.autistici.org/id/go-sso/proxy"
)

var (
	addr       = flag.String("addr", ":5003", "address to listen on")
	configFile = flag.String("config", "/etc/sso/proxy.yml", "path of config file")
)

func loadConfig() (*proxy.Config, error) {
	// Read YAML config.
	data, err := ioutil.ReadFile(*configFile)
	if err != nil {
		return nil, err
	}
	var config proxy.Config
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	h, err := proxy.NewProxy(config)
	if err != nil {
		log.Fatal(err)
	}

	srv := &http.Server{
		Addr:    *addr,
		Handler: h,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		_ = srv.Shutdown(ctx)
		_ = srv.Close()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	if err := srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
