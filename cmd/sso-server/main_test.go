package main

import (
	"io/ioutil"
	"os"
	"testing"
)

var testConfig = `---
secret_key_file: "/etc/sso/secret.key"
public_key_file: "/etc/sso/public.key"
domain: "example.com"
allowed_services:
  - "^(login|panel|monitor|logs)\\.example.com/$"
  - "^\\d+\\.webmail\\.example.com/$"
allowed_exchanges:
  - src_regexp: "^www.example.com/webmail/\\d+/$"
  - dst_regexp: "^imap.example.com/$"
service_ttls:
  - regexp: "^www.example.com/webmail/\\d+/$"
    ttl: 43200
  - regexp: "^imap.example.com/$"
    ttl: 43200
  - regexp: ".*"
    ttl: 300
auth_session_lifetime: 43200
session_secrets:
  - "iNQcyp5neUmbrxoj4yfRVhGL8HYGKNWRIv7t5ZiTxXwnJqBJYIU0gQx+1ar7Hsn0"
  - "Xqphf9jjr/jZCk+m"
csrf_secret: "XLFtiymBU5p59K/IsqW/oh/5dfP4UC6JSNWMVeiQ8t8GjnB1rzusIFnyho5y4nE1"
auth_service: sso
device_manager:
  auth_key: "ffolt81h4CA5kEcwckXmuUUkchwKQmRAeWb1H6Kpzx3+uGqwrVpBfGwzRSYaeir1"
`

func TestMain_LoadConfig(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ioutil.WriteFile(dir+"/config.yml", []byte(testConfig), 0640)

	conf, err := loadConfig(dir + "/config.yml")
	if err != nil {
		t.Fatal("LoadConfig:", err)
	}
	if err := conf.Config.Compile(); err != nil {
		t.Fatal("Compile:", err)
	}
}
