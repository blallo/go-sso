package main

import (
	"flag"
	"io/ioutil"
	"log"

	"git.autistici.org/ai3/go-common/serverutil"
	"gopkg.in/yaml.v2"

	"git.autistici.org/id/go-sso/saml"
)

var (
	addr       = flag.String("addr", ":5004", "address to listen on")
	configFile = flag.String("config", "/etc/sso/saml.yml", "`path` of config file")
)

// Config wraps together the standard HTTP server config and the SAML
// service configuration.
type Config struct {
	SAMLConfig   *saml.Config             `yaml:"saml"`
	ServerConfig *serverutil.ServerConfig `yaml:"http_server"`
}

func loadConfig() (*Config, error) {
	// Read YAML config.
	data, err := ioutil.ReadFile(*configFile)
	if err != nil {
		return nil, err
	}
	var config Config
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	s, err := saml.NewSAMLIDP(config.SAMLConfig)
	if err != nil {
		log.Fatal(err)
	}

	if err := serverutil.Serve(s, config.ServerConfig, *addr); err != nil {
		log.Fatal(err)
	}
}
